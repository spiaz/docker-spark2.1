FROM ubuntu:16.04

LABEL maintainer="Stefano Piazza"

# Set working directory in /
WORKDIR /app

# Install pip
RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y curl apt-utils zip && \
    apt-get install -y python3-pip && \
    pip3 --no-cache-dir install --upgrade pip && \
    ln -s /usr/bin/python3 /usr/bin/python

# Install & configure Java
RUN apt-get update && apt-get install software-properties-common -y && \
    add-apt-repository ppa:webupd8team/java && \
    echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
    echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections && \
    apt-get update && \
    apt-get install oracle-java8-installer -y
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle
ENV JRE_HOME /usr/lib/jvm/java-8-oracle/jre
ENV PATH "${PATH}:${JAVA_HOME}"
ENV PATH "${PATH}:${JRE_HOME}"

# Install Maven
RUN apt-get -y install maven

# Install & configure Scala
ENV SCALA_VERSION 2.11.8
RUN curl -fsL https://downloads.lightbend.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz | tar xfz - -C /opt/
ENV SCALA_HOME /opt/scala-$SCALA_VERSION
ENV PATH "${PATH}:${SCALA_HOME}/bin"

# Install & configure Spark
ENV SPARK_VERSION 2.1.0
RUN curl -fsL https://archive.apache.org/dist/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-hadoop2.7.tgz | tar xfz - -C /opt/
ENV SPARK_HOME /opt/spark-$SPARK_VERSION-bin-hadoop2.7
ENV PATH "${PATH}:${SPARK_HOME}/bin"

# Add and install requirements
COPY requirements.txt /app/requirements.txt
RUN pip3 --no-cache-dir install -r requirements.txt

# Apache Toree kernel
RUN pip3 install --no-cache-dir \
    https://dist.apache.org/repos/dist/dev/incubator/toree/0.2.0/snapshots/dev1/toree-pip/toree-0.2.0.dev1.tar.gz && \
    jupyter toree install --sys-prefix

# Install MLeap
# Install SBT (Necessary?? Maven should be enough)
RUN apt-get install apt-transport-https
RUN echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
RUN apt-get update
RUN apt-get -y install sbt
# Go on with MLeap installation

COPY kernel.json /usr/share/jupyter/kernels/apache_toree_scala/kernel.json
RUN pip3 install mleap
# RUN /usr/local/bin/spark-shell --packages ml.combust.mleap:mleap-spark_2.10:0.8.0

# Install other utilities I use often
RUN apt-get -y install locate vim fish less wget nodejs

EXPOSE 8888

VOLUME /app

CMD ["jupyter", "notebook", "--ip='*'", "--port=8888", "--no-browser", \
    "--allow-root"]
